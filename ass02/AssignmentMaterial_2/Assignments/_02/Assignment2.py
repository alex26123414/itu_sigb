#<!--------------------------------------------------------------------------->
#<!--                   ITU - IT University of Copenhagen                   -->
#<!--                   SSS - Software and Systems Section                  -->
#<!--           SIGB - Introduction to Graphics and Image Analysis          -->
#<!-- File       : Assignment2.py                                           -->
#<!-- Description: Main class of Assignment #02                             -->
#<!-- Author     : Fabricio Batista Narcizo                                 -->
#<!--            : Rued Langgaards Vej 7 - 4D06 - DK-2300 - Copenhagen S    -->
#<!--            : fabn[at]itu[dot]dk                                       -->
#<!-- Responsable: Dan Witzner Hansen (witzner[at]itu[dot]dk)               -->
#<!--              Fabricio Batista Narcizo (fabn[at]itu[dot]dk)            -->
#<!-- Information: No additional information                                -->
#<!-- Date       : 24/10/2015                                               -->
#<!-- Change     : 24/10/2015 - Creation of this class                      -->
#<!-- Review     : 29/03/2016 - Finalized                                   -->
#<!--------------------------------------------------------------------------->
from numpy.ma import vstack, ones, hstack, array
from numpy.matlib import eye


__version__ = "$Revision: 2016032901 $"

########################################################################
import cv2
import os
import numpy as np
import warnings

from pylab import draw
from pylab import figure
from pylab import plot
from pylab import show
from pylab import subplot

import SIGBTools
import Camera


########################################################################
class Assignment2(object):
    """Assignment2 class is the main class of Assignment #02."""

    #----------------------------------------------------------------------#
    #                           Class Attributes                           #
    #----------------------------------------------------------------------#
    __path = "./Assignments/_02/"

    #----------------------------------------------------------------------#
    #                    Assignment2 Class Constructor                     #
    #----------------------------------------------------------------------#
    def __init__(self):
        """Assignment2 Class Constructor."""
        warnings.simplefilter("ignore")

    #----------------------------------------------------------------------#
    #                         Public Class Methods                         #
    #----------------------------------------------------------------------#
    def Start(self):
        """Start Assignment #02."""
        option = "-1"
        clear  = lambda: os.system("cls" if os.name == "nt" else "clear")
        while option != "0":
            clear()
            print "\n\t#################################################################"
            print "\t#                                                               #"
            print "\t#                         Assignment #02                        #"
            print "\t#                                                               #"
            print "\t#################################################################\n"
            print "\t[1] Person Map Location. (2.01)"
            print "\t[2] Linear Texture Mapping (Ground Floor). (2.02)"
            print "\t[3] Linear Texture Mapping (Moving Objects). (2.03)"
            print "\t[4] Linear Texture Mapping (Ensuring a Correctly Placed Texture Map). (2.04)"
            print "\t[5] Image Augmentation on Image Reality."
            print "\t[6] Camera Calibration."
            print "\t[7] Augmentation."
            print "\t[8] Example \"ShowImageAndPlot()\" method."
            print "\t[9] Example \"SimpleTextureMap()\" method."
            print "\t[0] Back.\n"
            option = raw_input("\tSelect an option: ")

            if option == "1":
                self.__ShowFloorTrackingData()
            elif option == "2":
                self.__TextureMapGroundFloor()
            elif option == "3":
                self.__TextureMapGridSequence()
            elif option == "4":
                self.__RealisticTextureMap()
            elif option == "5":
                self.__TextureMapObjectSequence()
            elif option == "6":
                self.__CalibrateCamera()
            elif option == "7":
                self.__Augmentation()
            elif option == "8":
                self.__ShowImageAndPlot()
            elif option == "9":
                self.__SimpleTextureMap()
            elif option != "0":
                raw_input("\n\tINVALID OPTION!!!")

    #----------------------------------------------------------------------#
    #                        Private Class Methods                         #
    #----------------------------------------------------------------------#
    def __ShowFloorTrackingData(self):
        # Load videodata.
        filename = self.__path + "Videos/ITUStudent.avi"
        SIGBTools.VideoCapture(filename, SIGBTools.CAMERA_VIDEOCAPTURE_640X480)

        SIGBTools.RecordingVideos(self.__path + "Outputs/MapLocation.wmv", size=(800, 348))

        # Load tracking data.
        dataFile = np.loadtxt(self.__path + "Inputs/trackingdata.dat")
        lenght   = dataFile.shape[0]

        # Define the boxes colors.
        boxColors = [(255, 0, 0), (0, 255, 0), (0, 0, 255)] # BGR.

        image  = SIGBTools.read()
        image2 = cv2.imread(self.__path + "Images/ITUMap.png")

        # Look up the file or create a new one
        HgmFile = self.__path + "Outputs/homography1.npy";
        if os.path.isfile(HgmFile):
            Hgm = np.load(HgmFile)
        else:
            Hgm, _ = SIGBTools.GetHomographyFromMouse(image2, image, -4)
            np.save(HgmFile, Hgm)

        # Read each frame from input video and draw the rectangules on it.
        for i in range(lenght):
            # Read the current image from a video file.
            image = SIGBTools.read()

            # Draw each color rectangule in the image.
            boxes = SIGBTools.FrameTrackingData2BoxData(dataFile[i, :])
            for j in range(3):
                box = boxes[j]
                cv2.rectangle(image, box[0], box[1], boxColors[j])

            # TODO find a better points (center of the box)
            box = boxes[1]
            X = np.array([box[0][0], box[0][1], 1])
            Xp =np.dot(Hgm,X)
            cv2.circle(image2, (int(Xp[0]/Xp[2]), int(Xp[1]/Xp[2])), 1, (0, 255, 0), -1)
            SIGBTools.write(image2)

            # Show the final processed image.
            cv2.imshow("Ground Floor", image)
            cv2.imshow("Map", image2)

            if cv2.waitKey(1) & 0xFF == ord("q"):
                break

        cv2.imwrite(self.__path + "Outputs/mapImage.png", image2)

        # Wait 2 seconds before finishing the method.
        cv2.waitKey(2000)

        SIGBTools.close()

        # Close all allocated resources.
        cv2.destroyAllWindows()
        SIGBTools.release()

    def __TextureMapGroundFloor(self):
        """Places a texture on the ground floor for each input image."""
        # Load videodata.
        filename = self.__path + "Videos/ITUStudent.avi"
        SIGBTools.VideoCapture(filename, SIGBTools.CAMERA_VIDEOCAPTURE_640X480)

        SIGBTools.RecordingVideos(self.__path + "Outputs/TextureMapGroundFloor.wmv", size=(320, 256))

        # Load tracking data.
        dataFile = np.loadtxt(self.__path + "Inputs/trackingdata.dat")
        lenght   = dataFile.shape[0]

        # Define the boxes colors.
        boxColors = [(255, 0, 0), (0, 255, 0), (0, 0, 255)] # BGR.

        image  = SIGBTools.read()
        image2 = cv2.imread(self.__path + "Images/ITULogo.png")

        # Look if we saved the Htg, if not then create a new one
        HtgFile = self.__path + "Outputs/homography2.npy";
        if os.path.isfile(HtgFile):
            Htg = np.load(HtgFile)
        else:
            Htg, _ = SIGBTools.GetHomographyFromMouse(image2, image, -4)
            np.save(HtgFile, Htg)

        # Read each frame from input video and draw the rectangules on it.
        for i in range(lenght):
            # Read the current image from a video file.
            image = SIGBTools.read()

            # Draw each color rectangule in the image.
            boxes = SIGBTools.FrameTrackingData2BoxData(dataFile[i, :])
            for j in range(3):
                box = boxes[j]
                cv2.rectangle(image, box[0], box[1], boxColors[j])

            # Show the final processed image.
            h, w    = image.shape[0:2]
            overlay = cv2.warpPerspective(image2, Htg, (w, h))
            alpha = 0.85
            beta = 1-alpha
            result  = cv2.addWeighted(image, alpha, overlay, beta, 0)

            # Write the frame to the result video file
            SIGBTools.write(result)

            cv2.imshow("Ground Floor", result)
            if cv2.waitKey(1) & 0xFF == ord("q"):
                break

        SIGBTools.close()

        # Wait 2 seconds before finishing the method.
        cv2.waitKey(2000)

        # Close all allocated resources.
        cv2.destroyAllWindows()
        SIGBTools.release()

    def __TextureMapGridSequence(self):
        """Skeleton for texturemapping on a video sequence."""
        # Load videodata.
        filename = self.__path + "Videos/Grid01.mp4"
        SIGBTools.VideoCapture(filename, SIGBTools.CAMERA_VIDEOCAPTURE_640X480)

        SIGBTools.RecordingVideos(self.__path + "Outputs/TextureMapGridSequence_Grid01.wmv", size=(960, 540))

        # Load texture mapping image.
        texture = cv2.imread(self.__path + "Images/ITULogo.png")
        texture = cv2.pyrDown(texture)

        texturePoints = []
        m, n = texture.shape[0:2]
        # Define corner points from image "image1".
        texturePoints.append([(0, 0), (n, 0), (n, m), (0, m)])

        # Define the number and ids of inner corners per a chessboard row and column.
        patternSize = (9, 6)
        idx = [0, 8, 45, 53]

        # Read each frame from input video.
        while True:
            # Read the current image from a video file.
            image = SIGBTools.read()
            # Blurs an image and downsamples it.
            image = cv2.pyrDown(image)

            # Finds the positions of internal corners of the chessboard.
            corners = SIGBTools.FindCorners(image, isDrawed=False)
            if corners is not None:
                mainCorners = [[
                    (corners[53][0][0], corners[53][0][1]),
                    (corners[53 - 8][0][0], corners[53 - 8][0][1]),
                    (corners[0][0][0], corners[0][0][1]),
                    (corners[8][0][0], corners[8][0][1])
                ]]

                # Convert to OpenCV format.
                points1 = np.array([[x, y] for (x, y) in texturePoints[0]])
                points2 = np.array([[x, y] for (x, y) in mainCorners[0]])

                # Calculate the homography.
                Hmt, mask = cv2.findHomography(points1, points2)

                h, w    = image.shape[0:2]
                overlay = cv2.warpPerspective(texture, Hmt, (w, h))
                alpha   = 0.5
                beta    = 1-alpha
                result  = cv2.addWeighted(image, alpha, overlay, beta, 0)

                image = result

            # Write the frame to the result video file
            SIGBTools.write(image)

            # Show the final processed image.
            cv2.imshow("Image", image)
            if cv2.waitKey(1) & 0xFF == ord("q"):
                break

        SIGBTools.close()

        # Wait 2 seconds before finishing the method.
        cv2.waitKey(2000)

        # Close all allocated resources.
        cv2.destroyAllWindows()
        SIGBTools.release()

    def __RealisticTextureMap(self):
        # Load videodata.
        filename = self.__path + "Videos/ITUStudent.avi"
        SIGBTools.VideoCapture(filename, SIGBTools.CAMERA_VIDEOCAPTURE_640X480)

        SIGBTools.RecordingVideos(self.__path + "Outputs/RealisticTextureMap.wmv", size=(320, 256))

        # Load tracking data.
        dataFile = np.loadtxt(self.__path + "Inputs/trackingdata.dat")
        lenght   = dataFile.shape[0]

        image  = SIGBTools.read()
        texture = cv2.imread(self.__path + "Images/ITULogo.png")
        mapITU = cv2.imread(self.__path + "Images/ITUMap.png")

        # Look up the file or create a new one
        HgmFile = self.__path + "Outputs/homography1.npy"
        if os.path.isfile(HgmFile):
            Hgm = np.load(HgmFile)
        else:
            Hgm, _ = SIGBTools.GetHomographyFromMouse(mapITU, image, -4)
            np.save(HgmFile, Hgm)


        # Htg = SIGBTools.GetHomographyTG(mapITU, Hgm, texture, scale=0.9)
        HtgFile = self.__path + "Outputs/homography3.npy"
        if os.path.isfile(HtgFile):
            Htg = np.load(HtgFile)
        else:
            Htg = SIGBTools.GetHomographyTG(mapITU, Hgm, texture, scale=0.6)
            np.save(HtgFile, Htg)

        # Define the boxes colors.
        boxColors = [(255, 0, 0), (0, 255, 0), (0, 0, 255)] # BGR.

        # Read each frame from input video and draw the rectangules on it.
        for i in range(lenght):
            # Read the current image from a video file.
            image = SIGBTools.read()

            # Draw each color rectangule in the image.
            boxes = SIGBTools.FrameTrackingData2BoxData(dataFile[i, :])
            for j in range(3):
                box = boxes[j]
                cv2.rectangle(image, box[0], box[1], boxColors[j])

            # Show the final processed image.
            h, w    = image.shape[0:2]
            overlay = cv2.warpPerspective(texture, Htg, (w, h))
            alpha = 0.5
            beta = 1-alpha
            result  = cv2.addWeighted(image, alpha, overlay, beta, 0)

            # Write the frame to the result video file
            SIGBTools.write(result)

            cv2.imshow("Ground Floor", result)
            if cv2.waitKey(1) & 0xFF == ord("q"):
                break

        SIGBTools.close()

        # Wait 2 seconds before finishing the method.
        cv2.waitKey(2000)

        # Close all allocated resources.
        cv2.destroyAllWindows()
        SIGBTools.release()

    def __TextureMapObjectSequence(self):
        """Poor implementation of simple TextureMap."""
        # Load videodata.
        filename = self.__path + "Videos/Scene01.mp4"
        SIGBTools.VideoCapture(filename, SIGBTools.CAMERA_VIDEOCAPTURE_640X480)
        drawContours = True

        # Load texture mapping image.
        texture = cv2.imread(self.__path + "Images/ITULogo.png")

        # Read each frame from input video.
        while True:
            # Jump for each 20 frames in the video.
            for t in range(20):
                # Read the current image from a video file.
                image = SIGBTools.read()

            # Try to detect an object in the input image.
            squares = SIGBTools.DetectPlaneObject(image)

            # Check the corner of detected object.
            for sqr in squares:
                # Do texturemap here!!!!
                # TODO
                pass

            # Draws contours outlines or filled contours.
            if drawContours and len(squares) > 0:
                cv2.drawContours(image, squares, -1, (0, 255, 0), 3)

            # Show the final processed image.
            cv2.imshow("Detection", image)
            if cv2.waitKey(1) & 0xFF == ord("q"):
                break

        # Wait 2 seconds before finishing the method.
        cv2.waitKey(2000)

        # Close all allocated resources.
        cv2.destroyAllWindows()
        SIGBTools.release()

    def __CalibrateCamera(self):
        """Main function used for calibrating a common webcam."""
        # Load the camera.
        cameraID = 0
        SIGBTools.VideoCapture(cameraID, SIGBTools.CAMERA_VIDEOCAPTURE_640X480_30FPS)

        # Calibrate the connected camera.

        #SIGBTools.calibrate()

        cameraMatrix = np.load ("Framework/VideoCaptureDevices/CalibrationData/Camera_0_cameraMatrix.npy")
        translationVectors = np.load ("Framework/VideoCaptureDevices/CalibrationData/Camera_0_tvecs.npy")
        rotationVectors = np.load ("Framework/VideoCaptureDevices/CalibrationData/Camera_0_rvecs.npy")
        imgPoints = np.load ("Framework/VideoCaptureDevices/CalibrationData/Camera_0_img_points.npy")
        objPoints = np.load ("Framework/VideoCaptureDevices/CalibrationData/Camera_0_obj_points.npy")
        distCoeffs = np.load ("Framework/VideoCaptureDevices/CalibrationData/Camera_0_distCoeffs.npy")
        image = cv2.imread("Framework/VideoCaptureDevices/CalibrationData/Camera_0_chessboard1.png")

       # setup camera
        points = objPoints[0].T
        points = vstack((points,ones(points.shape[1])))
        RT = hstack((cv2.Rodrigues(rotationVectors[0])[0], translationVectors[0]))
        # setup camera
        P = np.dot(cameraMatrix, RT)
        cam = Camera.Camera(P)
        x = cam.project(points)

        image = cv2.undistort(image, cameraMatrix, distCoeffs)
        for point in x.T:
            cv2.circle(image, (int(point[0]), int(point[1])), 10, (0, 255, 0), -1)

        cv2.imshow("Points", image)
        cv2.waitKey(0)

        # Close all allocated resources.
        cv2.destroyAllWindows()

# Close all allocated resources.
        SIGBTools.release()

    def __Augmentation(self):
        """Projects an augmentation object over the chessboard pattern."""
        method1 = True
        # Load the camera.
        cameraID = 0
        SIGBTools.VideoCapture(cameraID, SIGBTools.CAMERA_VIDEOCAPTURE_640X480_30FPS)
        # SIGBTools.calibrate()

        calibrationPoints = np.load("./Framework/VideoCaptureDevices/CalibrationData/Camera_0_img_points.npy")
        homographyPoints = [0, 8, 45, 53]
        P, K, R, t, distCoeffs = SIGBTools.GetCameraParameters()

        # P: a 3x4 floating-point projection matrix.
        # K: a 3x3 floating-point camera matrix.
        # R: vector of rotation vectors estimated for each pattern view.
        # t: vector of translation vectors estimated for each pattern view.
        # distCoeffs: vector of distortion coefficients of 4, 5, or 8 elements.

        # Read each frame from input camera.
        while True:
            # Read the current image from the camera.
            #image = cv2.imread("D:/ITU/Semester 3/IMAGE/ASS1/itu_sigb/ass02/AssignmentMaterial_2/Framework/VideoCaptureDevices/CalibrationData/Camera_0_chessboard1.png")
            image = SIGBTools.read()
            # Finds the positions of internal corners of the chessboard.
            corners = SIGBTools.FindCorners(image, False)

            ch = cv2.waitKey(1)
            if ch == ord("x"):
                method1 = not method1

            if corners is not None:
                realPoints = np.array([corners[index].flatten() for index in homographyPoints])

                P2_Method1 = SIGBTools.PoseEstimationMethod1(image, corners, homographyPoints, calibrationPoints, P, K)
                P2_Method2 = SIGBTools.PoseEstimationMethod2(corners, SIGBTools.CalculatePattern(), K, distCoeffs)

                print("P2_Method1");print(P2_Method1);print("P2_Method2");print(P2_Method2)

                if method1:
                    SIGBTools.SetCameraParameters(P2_Method1, K, R, t, distCoeffs)
                else:
                    SIGBTools.SetCameraParameters(P2_Method2, K, R, t, distCoeffs)
                SIGBTools.DrawCoordinateSystem(image)
                #SIGBTools.DrawAugmentedCube(image)

            # Show the final processed image.
            cv2.imshow("P2_Method1" if method1 else "P2_Method2", image)
            if cv2.waitKey(1) & 0xFF == ord("q"):
                break

        # Wait 2 seconds before finishing the method.
        cv2.waitKey(2000)

        # Close all allocated resources.
        cv2.destroyAllWindows()
        SIGBTools.release()

    def __ShowImageAndPlot(self):
        """A simple attempt to get mouse inputs and display images using pylab."""
        # Read the input image.
        image  = cv2.imread(self.__path + "Images/ITUMap.png")
        image2 = image.copy()

        # Make figure and two subplots.
        fig = figure(1)
        ax1 = subplot(1, 2, 1)
        ax2 = subplot(1, 2, 2)
        ax1.imshow(image)
        ax2.imshow(image2)
        ax1.axis("image")
        ax1.axis("off")

        # Read 5 points from the input images.
        points = fig.ginput(5)
        fig.hold("on")

        # Draw the selected points in both input images.
        for point in points:
            # Draw on matplotlib.
            subplot(1, 2, 1)
            plot(point[0], point[1], "rx")
            # Draw on opencv.
            cv2.circle(image2, (int(point[0]), int(point[1])), 10, (0, 255, 0), -1)

        # Clear axis.
        ax2.cla()
        # Show the second subplot.
        ax2.imshow(image2)
        # Update display: updates are usually deferred.
        draw()
        show()
        # Save with matplotlib and opencv.
        fig.savefig(self.__path + "Outputs/imagePyLab.png")
        cv2.imwrite(self.__path + "Outputs/imageOpenCV.png", image2)

    def __SimpleTextureMap(self):
        """Example of how linear texture mapping can be done using OpenCV."""
        # Read the input images.
        image1 = cv2.imread(self.__path + "Images/ITULogo.png")
        image2 = cv2.imread(self.__path + "Images/ITUMap.png")

        # Estimate the homography.
        H, points = SIGBTools.GetHomographyFromMouse(image1, image2, 4)

        # Draw the homography transformation.
        h, w    = image2.shape[0:2]
        overlay = cv2.warpPerspective(image1, H, (w, h))
        result  = cv2.addWeighted(image2, 0.5, overlay, 0.5, 0)

        # Show the result image.
        cv2.imshow("SimpleTextureMap", result)
        cv2.waitKey(0)

        # Close all allocated resources.
        cv2.destroyAllWindows()