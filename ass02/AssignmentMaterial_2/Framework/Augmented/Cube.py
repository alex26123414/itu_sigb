#<!--------------------------------------------------------------------------->
#<!--                   ITU - IT University of Copenhagen                   -->
#<!--                   SSS - Software and Systems Section                  -->
#<!--           SIGB - Introduction to Graphics and Image Analysis          -->
#<!-- File       : Cube.py                                                  -->
#<!-- Description: Class used for creating an augmented cube                -->
#<!-- Author     : Fabricio Batista Narcizo                                 -->
#<!--            : Rued Langgaards Vej 7 - 4D06 - DK-2300 - Copenhagen S    -->
#<!--            : fabn[at]itu[dot]dk                                       -->
#<!-- Responsable: Dan Witzner Hansen (witzner[at]itu[dot]dk)               -->
#<!--              Fabricio Batista Narcizo (fabn[at]itu[dot]dk)            -->
#<!-- Information: No additional information                                -->
#<!-- Date       : 18/04/2015                                               -->
#<!-- Change     : 18/04/2015 - Creation of these classes                   -->
#<!--            : 05/12/2015 - Update the class for the new assignment     -->
#<!-- Review     : 05/12/2015 - Finalized                                   -->
#<!--------------------------------------------------------------------------->
from scipy import linalg

# <!--------------------------------------------------------------------------->
# <!--                   ITU - IT University of Copenhagen                   -->
# <!--                   SSS - Software and Systems Section                  -->
# <!--           SIGB - Introduction to Graphics and Image Analysis          -->
# <!-- File       : Cube.py                                                  -->
# <!-- Description: Class used for creating an augmented cube                -->
# <!-- Author     : Fabricio Batista Narcizo                                 -->
# <!--            : Rued Langgaards Vej 7 - 4D06 - DK-2300 - Copenhagen S    -->
# <!--            : fabn[at]itu[dot]dk                                       -->
# <!-- Responsable: Dan Witzner Hansen (witzner[at]itu[dot]dk)               -->
# <!--              Fabricio Batista Narcizo (fabn[at]itu[dot]dk)            -->
# <!-- Information: No additional information                                -->
# <!-- Date       : 18/04/2015                                               -->
# <!-- Change     : 18/04/2015 - Creation of these classes                   -->
# <!--            : 05/12/2015 - Update the class for the new assignment     -->
# <!-- Review     : 05/12/2015 - Finalized                                   -->
# <!--------------------------------------------------------------------------->
from ass02.AssignmentMaterial_2.Assignments._02 import Camera
from Framework.VideoCaptureDevices.CaptureManager import CaptureManager

__version__ = '$Revision: 2015051201 $'

########################################################################
import cv2
import numpy as np
import math


########################################################################
class Cube(object):
    """Cube Class is used creating an augmented cube."""

    # ----------------------------------------------------------------------#
    #                           Class Properties                           #
    # ----------------------------------------------------------------------#
    @property
    def Object(self):
        """Get an augmented cube."""
        return self.__Object

    @property
    def CoordinateSystem(self):
        """Get the augmented coordinate system."""
        return self.__CoordinateSystem

    # ----------------------------------------------------------------------#
    #                      Augmented Class Constructor                     #
    # ----------------------------------------------------------------------#
    def __init__(self):
        """Augmented Class Constructor."""
        # Creates the augmented objects used by this class.
        self.__CreateObjects()

    # ----------------------------------------------------------------------#
    #                         Public Class Methods                         #
    # ----------------------------------------------------------------------#
    def PoseEstimationMethod1(self, image, corners, homographyPoints, calibrationPoints, projectionMatrix,
                              cameraMatrix):
        """This method uses the homography between two views for finding the extrinsic parameters (R|t) of the camera in the current view."""
        # factor first 3*3 part
        calibrationPoints = np.array([calibrationPoints[0][index].flatten() for index in homographyPoints])
        corners = np.array([corners[index].flatten() for index in homographyPoints])

        for point in corners:
            cv2.circle(image, (point[0], point[1    ]), 10, (0, 255, 0), -1)

        H10 = cv2.findHomography(calibrationPoints, corners)[0]

        P = np.dot(H10, projectionMatrix)
        A = np.dot(linalg.inv(cameraMatrix), P[:,:3])
        A = np.array([A[:,0],A[:,1],np.cross(A[:,0].flatten(),A[:,1].flatten())]).T
        P[:,:3] = np.dot(cameraMatrix,A)

        return P

    def PoseEstimationMethod2(self, corners, patternPoints, cameraMatrix, distCoeffs):
        """This function uses the chessboard pattern for finding the extrinsic parameters (R|T) of the camera in the current view."""
        found, rvec, tvec = cv2.solvePnP(patternPoints, corners, cameraMatrix, distCoeffs)
        RT = np.hstack((cv2.Rodrigues(rvec)[0], tvec))
        # setup camera
        P = np.dot(cameraMatrix, RT)
        return P


    def DrawCoordinateSystem(self, image):
        """Draw the coordinate axes attached to the chessboard pattern."""
        origin = np.zeros((3, 1))
        origin = CaptureManager.Instance.Parameters.Project(origin)

        points = self.CoordinateSystem
        points = CaptureManager.Instance.Parameters.Project(points)

        cv2.line(image, tuple(origin[0].astype(int))[:2], tuple(points[0].astype(int))[:2], (255, 0, 0), 3)
        cv2.line(image, tuple(origin[0].astype(int))[:2], tuple(points[1].astype(int))[:2], (0, 255, 0), 3)
        cv2.line(image, tuple(origin[0].astype(int))[:2], tuple(points[2].astype(int))[:2], (0, 0, 255), 3)

        cv2.circle(image, tuple(points[0].astype(int))[:2], 2, (255, 0, 0), 12)
        cv2.circle(image, tuple(points[1].astype(int))[:2], 2, (0, 255, 0), 12)
        cv2.circle(image, tuple(points[2].astype(int))[:2], 2, (0, 0, 255), 12)

    def DrawAugmentedCube(self, image):
        """Draw a cube attached to the chessboard pattern."""
        points = self.Object
        points = CaptureManager.Instance.Parameters.Project(points)

        cv2.drawContours(image, [points[:4, :2].astype(int)], -1, (0, 255, 0), cv2.FILLED)

        for i, j in zip(range(4), range(4, 8)):
            cv2.line(image, tuple(points[i, :2].astype(int)), tuple(points[j, :2].astype(int)), (255, 0, 0), 3)

        cv2.drawContours(image, [points[4:, :2].astype(int)], -1, (0, 0, 255), 3)

    # ----------------------------------------------------------------------#
    #                         Private Class Methods                        #
    # ----------------------------------------------------------------------#
    def __CreateObjects(self):
        """Defines the points of the augmented objects based on calibration patterns."""
        # Creates an augmented cube.
        self.__Object = np.float32([[3, 1, 0], [3, 4, 0], [6, 4, 0], [6, 1, 0],
                                    [3, 1, -3], [3, 4, -3], [6, 4, -3], [6, 1, -3]]).T
        # Creates the coordinate system.
        self.__CoordinateSystem = np.float32([[2, 0, 0], [0, 2, 0], [0, 0, -2]]).reshape(-1, 3)

    # ----------------------------------------------------------------------#
    #                            Class Methods                             #
    # ----------------------------------------------------------------------#
    def __repr__(self):
        """Get a object representation in a string format."""
        return "Framework.Augumented.Cube object."