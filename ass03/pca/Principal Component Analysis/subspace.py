import numpy as np
import util

def pca (X, y, number_of_components = 0):
    X = util.asRowMatrix(X)
    [n,d] = X.shape
    if (number_of_components <= 0) or (number_of_components > n):
        number_of_components = n
    # <003> implement pca steps explained in the algorithm
    mu = np.mean(X, 0)
    X = X-mu
    cov = np.dot(X, X.T)

    eigenvalues, eigenvectors = np.linalg.eigh(cov)
    eigenvectors = np.dot(X.T, eigenvectors)
    for i in xrange(n):
        eigenvectors[:,i] = eigenvectors[:,i]/np.linalg.norm(eigenvectors[:,i])


    # <004> sort eigenvectors descending by their eigenvalue
    # we are slicing backwards


    # <005> select only number_of_components
    eigenvalues = eigenvalues[::-1][:number_of_components]
    eigenvectors = eigenvectors[:,::-1][:,:number_of_components]

    return [eigenvalues, eigenvectors, mu]



def project(W, X, mu=None):
    if mu is None :
        return np.dot(X,W)
    return np.dot(X - mu , W)



def reconstruct(W, Y, mu=None):
    if mu is None :
        return np.dot(Y,W.T)
    return np.dot(Y, W.T) + mu