import random
import sys

import matplotlib
import numpy as np
from subspace import pca, project, reconstruct
from util import normalize , asRowMatrix , read_images
from plot import subplot
import matplotlib.cm as cm

# <001> read images
X, y = read_images('data/')

# <002> perform a full pca
Y, W, mu = pca(X, y)

# turn the first (at most ) 16 eigenvectors into grayscale
# images ( note : eigenvectors are stored by column !)
E = []
for i in xrange(min(len(X), 16)):
    e = W[:,i].reshape(X[0].shape)
    E.append(normalize(e ,0 ,255))
    
# plot
subplot(title ="Eigenfaces", images = E, rows = 4, cols = 4, sptitle = "Eigenface",
          colormap = cm.gray , filename = None)

# <006> use different number of eigen faces to construct a face from dataset
# How many components do you think are enough for realistic reconstruction of a face?
# (select a face in the data set)
face = random.randint(0, len(X)-1)
face = 28
weights = project(W, np.array(X[face]).flatten())

E = []
sptitles = []
for i in xrange(min(len(X), 16)):
    e = reconstruct(W[:,:i*10], weights[:i*10], mu).reshape(X[0].shape)
    E.append(normalize(e ,0 ,255))
    sptitles.append(i*10)

# plot reconstructed face
subplot(title = "Reconstruction", images = E, rows = 4, cols = 4, sptitles = sptitles, sptitle = "Eigenvectors",
        colormap =cm.gray , filename = None)