from pylab import *
import cv2

#----------------------------Functions
def GetCoordinates(I):
    fig = figure(1)
    gray()
    imshow(I)

    fig.hold('on')
    X = ginput(2)
    print list(X)

    for (x, y) in X:
        plt.plot(x, y)

    show(False)
    return X


def showFigures(**imgs):
    figure()
    gray()

    for (counter, (k,v)) in enumerate(imgs.items()):
        subplot(1,len(imgs) ,counter+1)
        imshow(v)
        title(k)

    show()


# (b)Make a function called ExtractChannels(I) that gets an image I and returns the
# matrices of each three color channels.
def ExtractChannels(I):
    R = I[:, :, 0]
    G = I[:, :, 1]
    B = I[:, :, 2]
    return R, G, B

#----------------------------------Main body
I = cv2.imread("exercises02/Flag.jpg")
I = cv2.cvtColor(I, cv2.COLOR_RGB2BGR)
# # (a)
# print I.shape
#
# # (b)
# (red, green, blue) = ExtractChannels(I)
# showFigures(flag=I, flag_R=red, flag_G=green, flag_B=blue)

# (c)
# GetCoordinates(I)
# [(195.75806451612897, 72.935483870967914), (327.80039011703508, 144.91547464239272)]

# (d)
(y1, y2) = (74, 149)
(x1, x2) = (196, 329)
dkFlag = I[y1:y2, x1:x2, :]
dkR, dkG, dkB = ExtractChannels(dkFlag)
# showFigures(flag=dkFlag, flag_R=dkR, flag_G=dkG, flag_B=dkB)

# (e), (f)
whiteArea = nonzero(dkB >= 150)
borholmFlag = copy(dkFlag)
for i in range(len(whiteArea[0])):
    x = whiteArea[0][i]
    y = whiteArea[1][i]
    borholmFlag[x][y] = [0, 150, 100]
# showFigures(dk=dkFlag, borholm=borholmFlag)

# (g)
borI = copy(I)
borI[y1:y2, x1:x2, :] = borholmFlag

showFigures(initial=I, borholm=borI)
