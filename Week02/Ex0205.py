from pylab import *
import cv2

#----------------------------Functions
#When a double-starred parameter is declared such as $**imgs$,
#then all the keyword arguments from that point till the end are collected as a dictionary called $'imgs'$.


def showFigures(**imgs):
    figure()
    gray()

    for (counter, (k,v)) in enumerate(imgs.items()): #See how to use the $enumerate()$ function for creating a counter in a loop.
        subplot(1, len(imgs), counter+1)
        imshow(v)
        title(k)

    show()


# (c)
def DarkPupil(matrix, thr):
    A = copy(matrix)
    B = nonzero(matrix >= thr)
    for i in range(len(B[0])):
        x = B[0][i]
        y = B[1][i]
        A[x][y] = 255
    return A


# (d)
def BrightPupil(I, thr):
    A = copy(I)
    B = nonzero(I <= thr)
    for i in range(len(B[0])):
        x = B[0][i]
        y = B[1][i]
        A[x][y] = 0
    return A


# (f) - TODO
def Glint(I, thr):
    return None

# ---------------------- Main body

I = cv2.imread("exercises02/interlacedEye.jpg")  # Loading an image using openCV

# (b)
dark = copy(I)[::2]
bright = copy(I)[1::2]

dark = cv2.resize(dark, (I.shape[1], I.shape[0]))
bright = cv2.resize(bright, (I.shape[1], I.shape[0]))

# showFigures(Original_image=I, Dark_image=dark, Bright_image=bright)

# (c), (d), (e)
darkP = DarkPupil(dark, 40)
brightP = BrightPupil(bright, 60)

showFigures(Original_image=I, Dark_image=darkP, Bright_image=brightP)