
def CreateNameList(I):# Defining a function
    #...Write the code here
    l = []
    stop = False
    while not stop:
        name = raw_input("name  = ")
        if name.lower() == 'stop' :
            break
        else:
            l.append(name)

    a = 2*I
    return l; # You can return an object like a

def CreateNameList2(I):# Defining a function
    #...Write the code here
    l = []
    stop = False
    while not stop:
        name = input("name  = ")
        if name.lower() == 'stop' :
            break
        else:
            l.append(name)

    a = 2*I
    return l; # You can return an object like a


#-----------------------------------------------------Main body


I=3
# print CreateNameList(I) #Call function

# (c)
a = [1, 2, "AB"]
b = [4, 5, "CD"]

c = [a[0] + b[0], a[1] + b[1], a[2] + b[2]]
print "c   = " + str(c)
print "a+b = " + str(a+b)

a.append(b)

print "a = " + str(a)
print "a[3][2] = " + str(a[3][2])

print "a[3][1] = " + str(a[3][1])
print "a[2]    = " + str(a[2])

