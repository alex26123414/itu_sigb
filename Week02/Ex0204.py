# (b) (Extra) Make a function FindNumbers(L) that uses in operator to find all numbers in a
# list L, such that FindNumbers([1, 'a', 3, 15, 'b', 0]) returns [1, 3, 15, 0].

def findNumbers(L):
    return [n for n in L if unicode(str(n), 'utf-8').isnumeric()]

# (c) (Extra) Make a function called RemoveNonMP3(L) that locates all non-.mp3 files and
# removes them from a list like: fileList1 = ["1.mp4", "2.txt", "3.mp3", "4.wmv",
# "5.org", "6.mp3"]. Use the find() function.
def removeNonMP3(L):
    return [f for f in L if f.lower().find("mp3", f.rfind(".")) > 0]

# -------------- main ----------------

# # (b)
# print findNumbers([1, 'a', 3, 15, 'b', 0])

# # (c)
# print removeNonMP3(["1.mp3.mp4", "2.txt", "56.MP3", "3.mp3", "4.wmv", "5.org", "6.mp3"])

# # (d)
# import exercises02.RemoveNonMp3Files

# (e), (f)
from numpy import *

A = array([[1, 2, 100],
           [100, 100, 6]])
# B = (A == 100)
B = (A < 10)

print "B = \n%s\n" % B

(x, y) = nonzero(B)
print "x = ", x
print "y = ", y
