# # (a) Create a python file called sphereVolume.py in the exercises02 folder. The volume V of
# # a sphere with radius r is given by V = (see pdf) . For example a sphere with radius 5 has
# # a volume of approx 523.59. Write a program in Python that calculates the volume of a
# # sphere with a radius r and prints it in the screen.
# print ("-"*15) + " Ex 2.01 (a) " + ("-"*15)
# import numpy as np
#
# r = float(input("r="))
# V = (4 * np.pi * np.power(r, 3)) / 3
# print "V = %.2f" % V

# # (c)
# print ("-"*15) + " Ex 2.01 (c) " + ("-"*15)
# import math as m
#
# r = float(input("r="))
# V = (4 * m.pi * m.pow(r, 3)) / 3
# print "V = %.3f" % V

# # (e)
# print ("-"*15) + " Ex 2.01 (e) " + ("-"*15)
# import numpy as np
# try:
#     r = float(input("r="))
# except:
#     print "NaN. Invalid input."
#     exit()
#
# V = (4 * np.pi * np.power(r, 3)) / 3
# print "V = %.2f" % V

# (f)
print ("-"*15) + " Ex 2.01 (f) " + ("-"*15)
import numpy as np

valid = False
while not valid:
    try:
        r = float(input("r="))
        if r >= 0:
            valid = True
        else:
            print "Negative radius not OK."
    except:
        print "NaN. Invalid input."

V = (4 * np.pi * np.power(r, 3)) / 3
print "V = %.2f" % V