# (a)
def saveNameLists(n):
    f = open("names.txt", "w")
    while n>0:
        l = CreateNameList()
        f.write(str(l) + "\n")
        n -= 1
    f.close()


def CreateNameList():  # Defining a function
    l = []
    stop = False
    while not stop:
        name = raw_input("name  = ")
        if name.lower() == 'stop':
            break
        else:
            l.append(name)
    return l

# (c) TODO - make it with on for loop
def findNameList():
    f = open("names.txt", "r")
    all = []
    for ln in f:
        all.append(eval(ln))
    f.close()

    name = raw_input("What is the name to be found? ")
    for l in all:
        for nm in l:
            if nm == name:
                print l

# (d)
def readAndMakeDictionary():
    f = open("names.txt", "r")
    d = {}
    for ln in f:
        l = eval(ln)
        for name in l:
            tel = raw_input("Phone no for " + str(name) + ": ")
            d[name] = tel
    f.close()

    out = open("phones.txt", "w")
    for key in d.keys():
        out.write(key + ":" + d[key] + "\n")
    out.close()




# ----------- main -------------------
# saveNameLists(3)

# # (b)
# a1 = []
# a2 = []
# f = open("names.txt", "r")
# for ln in f:
#     a1.append(ln)
#     a2.append(eval(ln))
#
# typeA1 = str(type(a1[0]))
# typeA2 = str(type(a2[0]))
# print "a1 " + str(type(a1)) + typeA1 + " = " + str(a1)
# print "a2 " + str(type(a2)) + typeA2 + " = " + str(a2)

# # (c)
# findNameList()

# # (d)
# readAndMakeDictionary()

# (e)
nums = [1, 2, 3, 4]
fruit = ["Apples", "Peaches", "Pears", "Bananas"]
I = []
for n in nums:
    for f in fruit:
        if n % 2 == 0:
            I.append((n,f))
print I

I2 = [(i, f) for i in nums if i % 2 == 0 for f in fruit]
print I2