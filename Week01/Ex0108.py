# # (a)Read two integer numbers and print the biggest one
# a = int(input("a="))
# b = int(input("b="))
#
# print max(a,b)

# # (b) Read three integer numbers and print the biggest one
# a = int(input("a="))
# b = int(input("b="))
# c = int(input("c="))
#
# print max(a,b,c)

# # (c) Read the year of a car and write whether it is new or old
# from datetime import date
# year = int(input("Year of the car = "))
# acc_year = date.today().year
# if (acc_year-year < 3):
#     print "The car is NEW"
# else:
#     print "The car is OLD"

# # (d) Read one integer number and print whether is odd or even
# a = int(input("a="))
# if(a % 2 == 0):
#      print "EVEN"
# else:
#      print "ODD"

# # (e) Read your name and your birth year, and print your name and your probably age
# from datetime import date
# name = raw_input("What is your name? \n")
# byear = int(input("What is your birth year? \n"))
# age = date.today().year - byear
#
# print name + ", you are " + str(age) + " years old."

# # (f) Print all odd numbers between 0 and n
# n = int(input("n="))
# print range(1, n, 2)

# # (g) Create the variables A = 10 and B = 5 and change the values between them. Print the final result.
# A = 10
# B = 5
# temp = A
# A = B
# B = temp
# print "A=" + str(A)
# print "B=" + str(B)

# # (h) Read the dimensions of a two-dimensional rectangle (i.e. length and width). Calculate and print its perimeter and area
# L = float(input("L="))
# W = float(input("W="))
# print "P= %.2f" % (2*(L+W))
# print "A= %.2f" % (L*W)

# # (i) Print all numbers between 1 (inclusive) and 50 (inclusive) in decreasing order
# print range(50, 0, -1)

# (j) Create a list with 5 integer numbers, find and print the index of the smallest one.
l = [1,2,-3,4,5]
print l.index(min(l))
