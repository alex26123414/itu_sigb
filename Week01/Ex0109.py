# (a) Read an image: Use the function cv2.imread() to read an image. The image should
# be in the working directory or a full path of image should be given.

# import numpy as np
# import cv2
#
# # img = cv2.imread("img.jpg", cv2.IMREAD_GRAYSCALE)
# img = cv2.imread("img.jpg", cv2.IMREAD_GRAYSCALE)
# cv2.imwrite("img_gray.jpg", img)
# cv2.namedWindow("image", cv2.WINDOW_AUTOSIZE)
# cv2.imshow("image", img)
# cv2.waitKey(-1)
# cv2.destroyAllWindows()

# # (d) Sum it up: In Figure 7, the program loads an image in grayscale, displays it, save the
# # image if you press 's' and exit, or simply exit without saving if you press 'ESC' key
#
# import numpy as np
# import cv2
# import time
#
# img = cv2.imread("img.jpg", cv2.IMREAD_GRAYSCALE)
# cv2.imshow("Color lemon", img)
# k = cv2.waitKey(0) & 0xFF
# if k == 27:
#     cv2.destroyAllWindows()
# elif k == ord('s'):
#     t = time.localtime()
#     cv2.imwrite("img_" + time.strftime("%H%M%S", t) + ".jpg", img)
#     cv2.destroyAllWindows()

# (e) - (g)

import numpy as np
import cv2

cap = cv2.VideoCapture(0)

# The codec for the VideoWrtObj
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter("out.avi", fourcc, 20.0, (640,480))

while True:
    ret, frame = cap.read()
    frame = cv2.flip(frame, 0)

    gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)

    out.write(frame)

    cv2.imshow("frame", gray)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
out.release()
cv2.destroyAllWindows()
