from pylab import *
import cv2 

# Functions
def Show1_OpenCV(image):
    # This function define a window by namedWindow() and then show the image in that window.
    image = array(image)
    # Create a window called "By OpenCV" using the cv2.WINDOW_AUTOSIZE parameter and display the
    # image with its actual size in the window.
    cv2.namedWindow("By OpenCV", cv2.WINDOW_AUTOSIZE)
    # Show the image in "By OpenCV" window.
    cv2.imshow("By OpenCV", image)
    # The window will be closed with a (any) key press.
    cv2.waitKey(0)

def Show2_OpenCV(*images):
    # Showing the image using OpenCV
    image = []
    for i in images:
        image.append(array(i))
    # Show the image in "1" window.
    cv2.namedWindow("1", cv2.WINDOW_AUTOSIZE)
    cv2.imshow("1", image[0])
    # Show the image in "2" window.
    cv2.namedWindow("2", cv2.WINDOW_AUTOSIZE)
    cv2.imshow("2", image[1])
    # The window will be closed with a (any) key press.
    cv2.waitKey(0)

def Show1_PyLab(image):
    # Showing the image using pylab.
    figure("By pylab")
    gray()
    title("1");
    imshow(image)
    show()

def Show2_PyLab(*images):
    # Showing the image using pylab.
    figure("By pylab")
    gray()
    # More about "subplot()":
    # <http://www.scipy.org/Cookbook/Matplotlib/Multiple_Subplots_with_One_Axis_Label>
    subplot(1, 2, 1); title("1"); imshow(images[0])
    subplot(1, 2, 2); title("2"); imshow(images[1])
    show()

# Main body
I1 = cv2.imread("children.tif") 
