import cv2
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.path as path
import Test1
from pylab import *

# Functions
def DisplayHistogram(n, bins):
    # Create the plot structure.
    fig = plt.figure()
    ax  = fig.add_subplot(111)

    # Get the corners of the rectangles for the histogram.
    left   = np.array(bins[:-1])
    right  = np.array(bins[1:])
    bottom = np.zeros(len(left))
    top    = bottom + n

    # We need a (numrects x numsides x 2) numpy array for the path helper
    # function to build a compound path.
    XY = np.array([[left, left, right, right], [bottom, top, top, bottom]]).T

    # Get the path object.
    barpath = path.Path.make_compound_path_from_polys(XY)

    # Make a patch out of it.
    patch = patches.PathPatch(barpath, facecolor = 'blue', edgecolor = 'gray', alpha = 0.8)
    ax.add_patch(patch)

    # Update the view limits.
    ax.set_xlim(left[0], right[-1])
    ax.set_ylim(bottom.min(), top.max())

    # Show the final plot.
    plt.show()

# Main body
# Loading an image.
I = cv2.imread("colorProblem.jpg")
# Creating the image histogram.
n, bins = histogram(array(I), 256, normed = True)
# Displaing the histogram.
DisplayHistogram(n, bins)

# Example of histogram equalization.
# Loading an image.
I = cv2.imread("colorProblem.jpg", 0)
I_eq = cv2.equalizeHist(I)
Test1.Show2_OpenCV(I, I_eq)
