from pylab import *
import cv2 

# Functions
def ShowAll_PyLab(**images):
    # Showing the images using PyLab.
    figure()
    gray()
    # Create the dictionary of images.
    for (counter, (k, v)) in enumerate(images.items()):
        subplot(1, len(images), counter+1); imshow(v); title(k)
    show()
    
# Main body
# Loading the images  
I = cv2.imread("Eye1.jpg", 0)

# Binary Thresholding
(thresh, im_bw1) = cv2.threshold(I, 100, 255,cv2.THRESH_BINARY)
# Otsu Thresholding
(thresh, im_bw2) = cv2.threshold(I, 128, 255, cv2.THRESH_OTSU)
# Adaptive Thresholding
im_bw3 = cv2.adaptiveThreshold(I, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 113, 30)

ShowAll_PyLab(I1 = im_bw1, I2 = im_bw2, I3 = im_bw3)
