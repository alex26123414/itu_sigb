import cv2
cap = cv2.VideoCapture("exercises03/interlacedEye.mp4")
cv2.namedWindow("dark")
cv2.namedWindow("bright")
cv2.namedWindow("pupil")
cv2.namedWindow("THRESH_BINARY")
cv2.namedWindow("orig")
f = True
while f:
    f, img = cap.read()
    if f:
        cv2.imshow("orig", img)
        # (b)
        dark = img[1::2]
        bright = img[::2]
        # (c)
        dark = cv2.resize(dark, (img.shape[1], img.shape[0]))
        bright = cv2.resize(bright, (img.shape[1], img.shape[0]))

        cv2.imshow("dark", dark)
        cv2.imshow("bright", bright)
        # (d)
        pupil = cv2.subtract(dark, bright)
        cv2.imshow("pupil", pupil)

        # (e)
        pupilG = cv2.cvtColor(pupil, cv2.COLOR_RGB2GRAY)
        _, pupilG = cv2.threshold(pupilG, 40, 255, cv2.THRESH_BINARY)  # _, on the begining is cuz this fct retuns 2 Obj

        cv2.imshow("THRESH_BINARY", pupilG)

        ch = cv2.waitKey(33)
        if ch == 32:  # Break by SPACE key
            break

cv2.destroyAllWindows()
