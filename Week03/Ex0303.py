from pylab import *
import cv2


# Functions
def Show1_OpenCV(image):
    # This function define a window by namedWindow() and then show the image in that window.
    image = array(image)
    # Create a window called "By OpenCV" using the cv2.WINDOW_AUTOSIZE parameter and display the
    # image with its actual size in the window.
    cv2.namedWindow("By OpenCV", cv2.WINDOW_AUTOSIZE)
    # Show the image in "By OpenCV" window.
    cv2.imshow("By OpenCV", image)
    # The window will be closed with a (any) key press.
    cv2.waitKey(0)


def Show2_OpenCV(*images):
    # Showing the image using OpenCV
    image = []
    for i in images:
        image.append(array(i))
    # Show the image in "1" window.
    cv2.namedWindow("1", cv2.WINDOW_AUTOSIZE)
    cv2.imshow("1", image[0])
    # Show the image in "2" window.
    cv2.namedWindow("2", cv2.WINDOW_AUTOSIZE)
    cv2.imshow("2", image[1])
    # The window will be closed with a (any) key press.
    cv2.waitKey(0)


def Show1_PyLab(image):
    # Showing the image using pylab.
    figure("By pylab")
    gray()
    title("1");
    imshow(image)
    show()


def Show2_PyLab(*images):
    # Showing the image using pylab.
    figure("By pylab")
    gray()
    # More about "subplot()":
    # <http://www.scipy.org/Cookbook/Matplotlib/Multiple_Subplots_with_One_Axis_Label>
    subplot(1, 2, 1); title("1"); imshow(images[0])
    subplot(1, 2, 2); title("2"); imshow(images[1])
    show()


# 1(d)
def ShowAll_OpenCV(**imgs):
    for (counter, (k,v)) in enumerate(imgs.items()):
        cv2.namedWindow(k, cv2.WINDOW_AUTOSIZE)
        cv2.imshow(k, v)
    cv2.waitKey(0)


# 1(f)
def ShowAll_PyLab(**imgs):
    figure("By pylab")
    gray()

    for (counter, (k,v)) in enumerate(imgs.items()):
        subplot(1,len(imgs) ,counter+1)
        imshow(v)
        title(k)

    show()


# (a) - TODO
def GrayLevelMap2(I, vector):
    res = copy(I)
    for x in range(len(res)):
        i=0
        for y in range(len(res[x])):
            res[x][y] = vector[i%256]
            i += 1
    return res


def DisplayFunc(vector):
    points = []
    for i in range(len(vector)):
        points.append((i, vector[i]))

    bins = range(256)
    n = vector

    plt.grid(None, "major", "both")
    plot(bins, n, "k-", linewidth=5)
    axis([-2, 256, -2, 256])
    plt.show()


# (c)
def f(x):
    func = (255 - x) * sqrt(100 / x + 1)
    return func

# ----------------- Main body -------------
# 1(a)
I1 = cv2.imread("exercises03/children.tif")
I2 = cv2.imread("exercises03/Eye1.jpg")
I3 = cv2.imread("exercises03/Marker1.jpg")

# Show1_OpenCV(I1)
# Show2_OpenCV(I2, I3)
# 1(b)
I1 = cv2.cvtColor(I1, cv2.COLOR_RGB2GRAY)
I2 = cv2.cvtColor(I2, cv2.COLOR_RGB2GRAY)
I3 = cv2.cvtColor(I3, cv2.COLOR_RGB2GRAY)

# 1(c), 1(e)
# Show1_OpenCV(I1)
# Show2_OpenCV(I2, I3)
# Show1_PyLab(I1)
# Show2_PyLab(I2, I3)

# 1(d)
# ShowAll_OpenCV(image1=I1, image2=I2, image3=I3)

# 1(f)
# ShowAll_PyLab(image1=I1, image2=I2, image3=I3)


# DisplayFunc(range(256, 0, -1))
v = range(256, 0, -1)
v1 = range(50, 256) + [255]*50
v2 = [255] * 75 + [1] * (256-75)
v3 = range(0, 100) + [0] * 50 + range(150, 256)
v4 = [0] * 150 + [50] * 50 + [100] * 56

ShowAll_PyLab(i1=I1, modif=GrayLevelMap2(I1, v))

