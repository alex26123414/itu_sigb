# <!--------------------------------------------------------------------------->
# <!--                   ITU - IT University of Copenhagen                   -->
# <!--                   SSS - Software and Systems Section                  -->
# <!--           SIGB - Introduction to Graphics and Image Analysis          -->
# <!-- File       : Assignment1.py                                           -->
# <!-- Description: Main class of Assignment #01                             -->
# <!-- Author     : Fabricio Batista Narcizo                                 -->
# <!--            : Rued Langgaards Vej 7 - 4D06 - DK-2300 - Copenhagen S    -->
# <!--            : fabn[at]itu[dot]dk                                       -->
# <!-- Responsable: Dan Witzner Hansen (witzner[at]itu[dot]dk)               -->
# <!--              Fabricio Batista Narcizo (fabn[at]itu[dot]dk)            -->
# <!-- Information: No additional information                                -->
# <!-- Date       : 24/02/2016                                               -->
# <!-- Change     : 24/02/2016 - Creation of this class                      -->
# <!-- Review     : 24/02/2016 - Finalized                                   -->
# <!--------------------------------------------------------------------------->
from operator import itemgetter

import math

__version__ = "$Revision: 2016022401 $"

########################################################################
import SIGBTools

import cv2
import os
import numpy as np
import time
import warnings

from matplotlib import pyplot as plt
from pylab import draw
from pylab import figure
from pylab import imshow
from pylab import plot
from pylab import show
from pylab import subplot
from pylab import title

from scipy.cluster.vq import kmeans
from scipy.cluster.vq import vq


########################################################################
class Assignment1(object):
    """Assignment1 class is the main class of Assignment #01."""

    # ----------------------------------------------------------------------#
    #                           Class Attributes                           #
    # ----------------------------------------------------------------------#
    __path = "./Assignments/_01/"

    # ----------------------------------------------------------------------#
    #                           Class Properties                           #
    # ----------------------------------------------------------------------#
    @property
    def OriginalImage(self):
        """Get the current original image."""
        return self.__OriginalImage

    @OriginalImage.setter
    def OriginalImage(self, value):
        """Set the current original image."""
        self.__OriginalImage = value

    @property
    def ResultImage(self):
        """Get the result image."""
        return self.__ResultImage

    @ResultImage.setter
    def ResultImage(self, value):
        """Set the result image."""
        self.__ResultImage = value

    @property
    def LeftTemplate(self):
        """Get the left eye corner template."""
        return self.__LeftTemplate

    @LeftTemplate.setter
    def LeftTemplate(self, value):
        """Set the left eye corner template."""
        self.__LeftTemplate = value

    @property
    def RightTemplate(self):
        """Get the right eye corner template."""
        return self.__RightTemplate

    @RightTemplate.setter
    def RightTemplate(self, value):
        """Set the right eye corner template."""
        self.__RightTemplate = value

    @property
    def FrameNumber(self):
        """Get the current frame number."""
        return self.__FrameNumber

    @FrameNumber.setter
    def FrameNumber(self, value):
        """Set the current frame number."""
        self.__FrameNumber = value

    # ----------------------------------------------------------------------#
    #                    Assignment1 Class Constructor                     #
    # ----------------------------------------------------------------------#
    def __init__(self):
        """Assignment1 Class Constructor."""
        warnings.simplefilter("ignore")
        self.OriginalImage = np.ones((1, 1, 1), dtype=np.uint8)
        self.ResultImage = np.ones((1, 1, 1), dtype=np.uint8)
        self.LeftTemplate = np.ones((1, 1, 1), dtype=np.uint8)
        self.RightTemplate = np.ones((1, 1, 1), dtype=np.uint8)
        self.FrameNumber = 0

    # ----------------------------------------------------------------------#
    #                         Public Class Methods                         #
    # ----------------------------------------------------------------------#
    def Start(self):
        """Start the eye tracking system."""
        # Show the menu.
        self.__Menu()
        # Read a video file.
        filename = raw_input("\n\tType a filename from \"Inputs\" folder: ")
        # filename = "eye01.avi"
        filepath = self.__path + "/Inputs/" + filename
        if not os.path.isfile(filepath):
            print "\tInvalid filename!"
            time.sleep(1)
            return

        # Show the menu.
        self.__Menu()

        # Define windows for displaying the results and create trackbars.
        self.__SetupWindowSliders()

        # Load the video file.
        SIGBTools.VideoCapture(filepath)

        # Shows the first frame.
        self.OriginalImage = SIGBTools.read()
        self.FrameNumber = 1
        self.__UpdateImage()

        # Initial variables.
        saveFrames = False

        # Read each frame from input video.
        while True:
            # Extract the values of the sliders.
            sliderVals = self.__GetSliderValues()

            # Read the keyboard selection.
            ch = cv2.waitKey(1)

            # Select regions in the input images.
            if ch is ord("m"):
                if not sliderVals["Running"]:
                    roiSelector = SIGBTools.ROISelector(self.OriginalImage)
                    points, regionSelected = roiSelector.SelectArea("Select left eye corner", (400, 200))
                    if regionSelected:
                        self.LeftTemplate = self.OriginalImage[points[0][1]:points[1][1], points[0][0]:points[1][0]]

            # Recording a video file.
            elif ch is ord("s"):
                if saveFrames:
                    SIGBTools.close()
                    saveFrames = False
                else:
                    resultFile = raw_input("\n\tType a filename (result.wmv): ")
                    resultFile = self.__path + "/Outputs/" + resultFile
                    if os.path.isfile(resultFile):
                        print "\tThis file exist! Try again."
                        time.sleep(1)
                        self.__Menu()
                        continue
                    elif not resultFile.endswith("wmv"):
                        print "\tThis format is not supported! Try again."
                        time.sleep(1)
                        self.__Menu()
                        continue
                    self.__Menu()
                    size = self.OriginalImage.shape
                    SIGBTools.RecordingVideos(resultFile, (size[1], size[0]))
                    saveFrames = True

            # Spacebar to stop or start the video.
            elif ch is 32:
                cv2.setTrackbarPos("Stop/Start", "Threshold", not sliderVals["Running"])

            # Restart the video.
            elif ch is ord("r"):
                # Release all connected videos/cameras.
                SIGBTools.release()
                time.sleep(0.5)

                # Load the video file.
                SIGBTools.VideoCapture(filepath)

                # Shows the first frame.
                self.OriginalImage = SIGBTools.read()
                self.FrameNumber = 1
                self.__UpdateImage()

            # Quit the eye tracking system.
            elif ch is 27 or ch is ord("q"):
                break

            # Check if the video is running.
            if sliderVals["Running"]:
                self.OriginalImage = SIGBTools.read()
                self.FrameNumber += 1
                self.__UpdateImage()

                if saveFrames:
                    SIGBTools.write(self.ResultImage)

        # Close all allocated resources.
        cv2.destroyAllWindows()
        SIGBTools.release()
        if saveFrames:
            SIGBTools.close()

    # ----------------------------------------------------------------------#
    #                        Private Class Methods                         #
    # ----------------------------------------------------------------------#
    def __Menu(self):
        """Menu of Assignment #01."""
        clear = lambda: os.system("cls" if os.name == "nt" else "clear")
        clear()
        print "\n\t#################################################################"
        print "\t#                                                               #"
        print "\t#                         Assignment #01                        #"
        print "\t#                                                               #"
        print "\t#################################################################\n"
        print "\t[R] Reload Video."
        print "\t[M] Mark the Region of Interested (RoI) when the video has paused."
        print "\t[S] Toggle video writing."
        print "\t[SPACE] Pause."
        print "\t[Q] or [ESC] Stop and Back.\n"

    # ----------------------------------------------------------------------#
    #                         Eye Features Methods                         #
    # ----------------------------------------------------------------------#
    def __GetPupil(self, grayscale, threshold, minSize, maxSize):
        """Given a grayscale level image and a threshold value returns a list of pupil candidates."""
        # Draw a red circle on coordinate 100x200.
        # cv2.circle(self.ResultImage, (100, 200), 2, (0, 0, 255), 4)

        # Create a binary image.
        val, threshold = cv2.threshold(grayscale, threshold, 255, cv2.THRESH_BINARY_INV)
        cv2.imshow("Threshold", threshold)

        # Get the blob properties.
        props = SIGBTools.RegionProps()

        # Calculate blobs.
        _, contours, hierarchy = cv2.findContours(threshold, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        pupils = []

        # YOUR IMPLEMENTATION HERE !!!!

        for cont in contours:
            p = props.CalcContourProperties(cont, ["Centroid", "Area", "Extend"])

            Area = p["Area"]
            Extend = p["Extend"]
            if minSize < Area < maxSize and Extend > 0.5:
                pupilEllipse = cv2.fitEllipse(cont)
                pupils.append(pupilEllipse)

        return pupils

    def __DetectPupilKMeans(self, grayscale, K=5, distanceWeight=10, reSize=(40, 40)):
        """Detects the pupil in the grayscale image using k-means.
           grayscale         : input grayscale image
           K                 : Number of clusters
           distanceWeight    : Defines the weight of the position parameters
           reSize            : the size of the image to do k-means on
        """
        # Resize for faster performance.
        smallI = cv2.resize(grayscale, reSize)
        M, N = smallI.shape

        # Generate coordinates in a matrix.
        X, Y = np.meshgrid(range(M), range(N))
        # Make coordinates and intensity into one vectors.
        z = smallI.flatten()
        x = X.flatten()
        y = Y.flatten()
        O = len(x)

        # Make a feature vectors containing (x, y, intensity).
        features = np.zeros((O, 3))
        features[:, 0] = z
        features[:, 1] = y / distanceWeight  # Divide so that the distance of position weighs less than intensity.
        features[:, 2] = x / distanceWeight
        features = np.array(features, "f")

        # Cluster data.
        centroids, variance = kmeans(features, K)
        # Use the found clusters to map.
        centroids.argmin()
        label, distance = vq(features, centroids)
        # Re-create image from.
        labelIm = np.array(np.reshape(label, (M, N)), dtype=np.uint8)

        # f = figure("K-Means")
        # imshow(labelIm)
        # f.canvas.draw()
        # f.show()

        val, threshold = min((val, idx) for (idx, val) in enumerate(centroids[:, [0]]))

        return labelIm, threshold, val

    def __FindEllipseContour(self, img, gradientMagnitude, gradientOrientation, estimatedCenter, estimatedRadius,
                             nPts=30):
        P = SIGBTools.GetCircleSamples(center=estimatedCenter, radius=
        estimatedRadius, numPoints=nPts)
        t = 0
        nPts = 40
        newPupil = []
        for (x, y, dx, dy) in P:
            # < Define normalLength as some maximum distance away from initial circle >
            normalLength = 80
            x = int(x)
            y = int(y)
            p1 = (x, y)
            p2 = (int(x + normalLength * dx), int(y + normalLength * dy))
            cv2.line(img, p1, p2, (0, 255, 0), 1)
            # < Get the endpoints of the normal -> p1 , p2 >
            maxPoint = self.__FindMaxGradientValueOnNormal(gradientMagnitude, gradientOrientation, p2, p1)
            cv2.circle(img, (tuple(maxPoint)), 2, (0, 0, 255), 5)
            newPupil.append(tuple(maxPoint))
            t = t + 1
        # < fitPoints to model using least squares - cv2. fitellipse (newPupil ) >
        pupil = cv2.fitEllipse(np.array(newPupil))
        cv2.ellipse(img, pupil, (0, 255, 0), 1)
        return pupil

    def __FindMaxGradientValueOnNormal(self, gradientMagnitude, gradientOrientation, p1, p2):
        # Get integer coordinates on the straight line between p1 and p2.
        pts = SIGBTools.GetLineCoordinates(p1, p2)
        normalVals = gradientMagnitude[pts[:, 1], pts[:, 0]]
        # Find index of max value in normalVals
        dx = (p1[0] - p2[0]) if (p1[0] - p2[0]) > 0 else 1
        angle = int(math.atan((p1[1] - p2[1]) / dx) * 180 / math.pi)
        prev = 0

        while True:
            val, idx = max((val, idx) for (idx, val) in enumerate(normalVals[:]))
            if math.fabs(gradientOrientation[pts[idx][1]][pts[idx][0]] - angle) < 50:
                return pts[idx]
            if gradientOrientation[pts[idx][1]][pts[idx][0]] - angle > gradientOrientation[pts[prev][1]][
                pts[prev][0]] - angle:
                return pts[idx]
            prev = idx
            if len(normalVals) == 1:
                return pts[idx]
            normalVals = np.delete(normalVals, idx)

    def __BlobDetection(self, labelIm, threshold, minP, maxP):

        blobs = []
        thresholded = np.ma.masked_not_equal(labelIm, threshold)

        # Get the blob properties.
        props = SIGBTools.RegionProps()

        # Calculate blobs.
        _, contours, hierarchy = cv2.findContours(thresholded, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

        for contour in contours:
            p = props.CalcContourProperties(contour, ["Centroid", "Area", "Extend"])
            Area = p["Area"]
            Extend = p["Extend"]
            if minP < Area < maxP and Extend > 0.5:
                pupilEllipse = cv2.fitEllipse(contour)
                blobs.append(pupilEllipse)
        # f = figure("Thresholded")
        # f.clear()
        # imshow(thresholded)
        # f.canvas.draw()
        # f.show()
        return blobs

    def __DetectPupilHough(self, grayscale):
        """Performs a circular hough transform in the grayscale image and shows the detected circles.
           The circle with most votes is shown in red and the rest in green colors."""
        # See help for http://docs.opencv.org/3.0-beta/modules/imgproc/doc/feature_detection.html?highlight=hough#cv2.HoughCircles
        blur = cv2.GaussianBlur(grayscale, (31, 31), 11)

        dp = 6  # Inverse ratio of the accumulator resolution to the image resolution.
        minDist = 30  # Minimum distance between the centers of the detected circles.
        highThr = 20  # High threshold for canny.
        accThr = 850  # Accumulator threshold for the circle centers at the detection stage. The smaller it is, the more false circles may be detected.
        minRadius = 50  # Minimum circle radius.
        maxRadius = 155  # Maximum circle radius.

        # Apply the hough transform.
        circles = cv2.HoughCircles(blur, cv2.HOUGH_GRADIENT, dp, minDist, None, highThr, accThr, minRadius, maxRadius)

        # Make a color image from grayscale for display purposes.
        results = cv2.cvtColor(grayscale, cv2.COLOR_GRAY2BGR)

        # Print all detected circles
        if circles is not None:
            # Print all circles.
            all_circles = circles[0]
            M, N = all_circles.shape
            k = 1
            for circle in all_circles:
                cv2.circle(results, tuple(circle[0:2]), circle[2], (int(k * 255 / M), k * 128, 0))
                circle = all_circles[0, :]
                cv2.circle(results, tuple(circle[0:2]), circle[2], (0, 0, 255), 5)
                k = k + 1

        # Return the result image.
        return results

    def __GetGlints(self, grayscale, threshold):
        """Given a grayscale level image and a threshold value returns a list of glint candidates."""
        glints = []

        # YOUR IMPLEMENTATION HERE !!!!

        # Create a binary image.
        val, threshold = cv2.threshold(grayscale, threshold, 255, cv2.THRESH_BINARY)
        cv2.imshow("Threshold", threshold)

        # Get the blob properties.
        props = SIGBTools.RegionProps()

        # Calculate blobs.
        _, contours, hierarchy = cv2.findContours(threshold, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

        for cont in contours:
            g = props.CalcContourProperties(cont, ["Centroid", "Area", "Extend", "Length"])

            Area = g["Area"]
            Length = g["Length"]
            if 60 < Area < 150:
                if 38 < Length < 45:
                    glints.append(g["Centroid"])

        return glints

    def __FilterPupilGlint(self, pupils, glints):
        """Given a list of pupil candidates and glint candidates returns a list of pupil and glints."""
        filteredPupils = []
        filteredGlints = []

        # YOUR IMPLEMENTATION HERE !!!!

        return filteredPupils, filteredGlints

    def __GetEyeCorners(self, leftTemplate, rightTemplate, pupilPosition=None):
        """Given two templates and the pupil center returns the eye corners position."""
        corners = []

        # YOUR IMPLEMENTATION HERE !!!!

        return corners

    def __GetIrisUsingThreshold(self, grayscale, pupils, threshold):
        """Given a gray level image and the pupil candidates returns a list of iris locations."""
        iris = []

        # YOUR IMPLEMENTATION HERE !!!!

        # Create a binary image.
        val, threshold = cv2.threshold(grayscale, threshold, 255, cv2.THRESH_BINARY)

        # Get the blob properties.
        props = SIGBTools.RegionProps()

        # Calculate blobs.
        _, contours, hierarchy = cv2.findContours(threshold, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

        for cont in contours:
            g = props.CalcContourProperties(cont, ["Centroid", "Area", "Extend"])
            gArea = g["Area"]
            gExtend = g["Extend"]
            for pupil in pupils:
                pArea = int(abs(pupil[0][0] - pupil[0][1]) * abs(pupil[1][0] - pupil[1][1]) * math.pi)
                if (pArea < gArea) and gExtend < 0.5:
                    irisEllipse = cv2.fitEllipse(cont)
                    iris.append(irisEllipse)
                    break

        return iris

    def __GetGradientImageInfo(self, I):
        gradient = np.gradient(I)
        orientation = np.zeros(I.shape)

        sobelx = cv2.Sobel(I, cv2.CV_64F, 1, 0, ksize=5)
        sobely = cv2.Sobel(I, cv2.CV_64F, 0, 1, ksize=5)

        for i in range(len(sobelx)):
            for j in range(len(sobelx[i])):
                orientation[i][j] = cv2.fastAtan2(sobelx[i][j], sobely[i][j])
        magnitude = cv2.magnitude(sobelx, sobely)

        plt.subplot(2, 3, 1), plt.imshow(I, cmap='gray')
        plt.title('Original'), plt.xticks([]), plt.yticks([])
        plt.subplot(2, 3, 3), plt.imshow(sobelx, cmap='gray')
        plt.title('Sobel X'), plt.xticks([]), plt.yticks([])
        plt.subplot(2, 3, 4), plt.imshow(sobely, cmap='gray')
        plt.title('Sobel Y'), plt.xticks([]), plt.yticks([])
        plt.subplot(2, 3, 5), plt.imshow(orientation, cmap='gray')
        plt.title('Orientation'), plt.xticks([]), plt.yticks([])
        plt.subplot(2, 3, 6), plt.imshow(magnitude, cmap='gray')
        plt.title('Magnitude'), plt.xticks([]), plt.yticks([])

        return orientation, magnitude

    def __CircleTest(self, I, pupils):
        cv2.namedWindow("circle")
        nPts = 20
        circles = []
        for pupil in pupils:
            P = SIGBTools.GetCircleSamples(center=pupil[0], radius=pupil[2] / 2, numPoints=nPts)
            circles.append(P)
        for circle in circles:
            for (x, y, dx, dy) in circle:
                cv2.line(I, (int(x + 100 * dx), int(y + 100 * dy)), (int(x), int(y)), (255, 255, 0), 1)
                cv2.circle(I, (int(x), int(y)), 2, (0, 255, 255), 5)

        cv2.imshow("circle", I)

    def __GetIrisUsingNormals(self, grayscale, pupil, normalLength):
        """Given a grayscale level image, the pupil candidates and the length of the normals returns a list of iris locations."""
        iris = []

        # YOUR IMPLEMENTATION HERE !!!!

        return iris

    def __GetIrisUsingSimplifyedHough(self, grayscale, pupil):
        """Given a grayscale level image and the pupil candidates returns a list of iris locations using a simplified Hough Transformation."""
        iris = []

        # YOUR IMPLEMENTATION HERE !!!!

        return iris

    # ----------------------------------------------------------------------#
    #                        Image Display Methods                         #
    # ----------------------------------------------------------------------#
    def __UpdateImage(self):
        """Calculate the image features and display the result based on the slider values."""
        # Show the original image.
        cv2.imshow("Original", self.OriginalImage)

        # Get a copy of the current original image.
        self.ResultImage = image = self.OriginalImage.copy()

        # Extract the values of the sliders.
        sliderVals = self.__GetSliderValues()

        # Perform the eye features detector.
        grayscale = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
        # st = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(10,10))
        # I_close = cv2.morphologyEx(grayscale, cv2.MORPH_CLOSE, st)
        imageK, threshold, val = self.__DetectPupilKMeans(grayscale)
        imageK = cv2.resize(imageK, dsize=(image.shape[1], image.shape[0]))
        pupils = self.__GetPupil(grayscale, sliderVals["pupilThr"], sliderVals["minSize"], sliderVals["maxSize"])
        # pupils = self.__GetPupil(grayscale, val, sliderVals["minSize"], sliderVals["maxSize"])
        #pupils = self.__BlobDetection(imageK, threshold, sliderVals["minSize"], sliderVals["maxSize"])
        # glints = self.__GetGlints(grayscale, sliderVals["glintThr"])
        iris = self.__GetIrisUsingThreshold(grayscale, pupils, sliderVals["irisThr"])
        # self.__FilterPupilGlint(pupils, glints)
        # glints=[]

        st = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (10, 10))
        I_close = cv2.morphologyEx(grayscale, cv2.MORPH_OPEN, st)
        orientation, magnitute = self.__GetGradientImageInfo(I_close)
        if len(pupils) > 0:
            self.__FindEllipseContour(image, magnitute, orientation, pupils[0][0], pupils[0][2] / 4)
            self.__FindEllipseContour(image, magnitute, orientation, pupils[0][0], pupils[0][2])
        # self.__CircleTest(grayscale, pupils)

        # Do template matching.
        leftTemplate = self.LeftTemplate
        rightTemplate = self.RightTemplate
        corners = self.__GetEyeCorners(leftTemplate, rightTemplate)

        ## For Iris Detection - Assignment #02 (Part 02)
        # iris = self.__CircularHough(grayscale)

        # Display results.
        x, y = 10, 20
        self.__SetText(image, (x, y), "Frame: %d" % self.FrameNumber)

        # Print the values of the threshold.
        step = 28
        self.__SetText(image, (x, y + step), "pupilThr :" + str(sliderVals["pupilThr"]))
        self.__SetText(image, (x, y + 2 * step), "glintThr :" + str(sliderVals["glintThr"]))
        self.__SetText(image, (x, y + 3 * step), "irisThr :" + str(sliderVals["irisThr"]))

        ## Uncomment these lines as your methods start to work to display the result.
        for pupil in pupils:
            cv2.ellipse(image, pupil, (0, 255, 0), 1)
            center = int(pupil[0][0]), int(pupil[0][1])
            cv2.circle(image, center, 2, (0, 0, 255), 4)

        # for glint in glints:
        #     center = int(glint[0]), int(glint[1])
        #     cv2.circle(image, center, 2, (255, 0, 255), 5)
        #
        # for ir in iris:
        #     cv2.ellipse(image, ir, (0, 255, 0), 1)
        #     center = int(ir[0][0]), int(ir[0][1])
        #     cv2.circle(image, center, 2, (0, 255, 0), 4)

        # Show the final processed image.
        cv2.imshow("Results", image)

    def __SetText(self, image, (x, y), string):
        """Draw a text on input image."""
        cv2.putText(image, string, (x + 1, y + 1), cv2.FONT_HERSHEY_PLAIN, 1.0, (0, 0, 0), thickness=2,
                    lineType=cv2.LINE_AA)
        cv2.putText(image, string, (x, y), cv2.FONT_HERSHEY_PLAIN, 1.0, (255, 255, 255), lineType=cv2.LINE_AA)

    # ----------------------------------------------------------------------#
    #                        Windows Events Methods                        #
    # ----------------------------------------------------------------------#
    def __SetupWindowSliders(self):
        """Define windows for displaying the results and create trackbars."""
        # Windows.
        cv2.namedWindow("Original")
        cv2.namedWindow("Threshold")
        cv2.namedWindow("Results")

        # Threshold value for the pupil intensity.
        cv2.createTrackbar("pupilThr", "Threshold", 90, 255, self.__OnSlidersChange)
        # Threshold value for the glint intensities.
        cv2.createTrackbar("glintThr", "Threshold", 240, 255, self.__OnSlidersChange)
        # Define the minimum and maximum areas of the iris.
        cv2.createTrackbar("irisThr", "Threshold", 90, 255, self.__OnSlidersChange)
        # Threshold value for the glint intensities.
        cv2.createTrackbar("minSize", "Threshold", 20, 200, self.__OnSlidersChange)
        cv2.createTrackbar("maxSize", "Threshold", 200, 200, self.__OnSlidersChange)
        # Value to indicate whether to run or pause the video.
        cv2.createTrackbar("Stop/Start", "Threshold", 0, 1, self.__OnSlidersChange)

    def __GetSliderValues(self):
        """Extract the values of the sliders and return these in a dictionary."""
        sliderVals = {}

        sliderVals["pupilThr"] = cv2.getTrackbarPos("pupilThr", "Threshold")
        sliderVals["glintThr"] = cv2.getTrackbarPos("glintThr", "Threshold")
        sliderVals["irisThr"] = cv2.getTrackbarPos("irisThr", "Threshold")
        sliderVals["minSize"] = 50 * cv2.getTrackbarPos("minSize", "Threshold")
        sliderVals["maxSize"] = 50 * cv2.getTrackbarPos("maxSize", "Threshold")
        sliderVals["Running"] = 1 == cv2.getTrackbarPos("Stop/Start", "Threshold")

        return sliderVals

    def __OnSlidersChange(self, dummy=None):
        """Handle updates when slides have changed.
           This method only updates the display when the video is put on pause."""
        sliderVals = self.__GetSliderValues()
        if not sliderVals["Running"]:
            self.__UpdateImage()
